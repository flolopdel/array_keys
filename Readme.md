# Array keys function

*"Implementa en PHP la función estándar array_keys (http://php.net/manual/en/function.array-keys.php). Se valorará que la **solución sea correcta** (debe funcionar exactamente igual que la array_keys estándar), **la claridad del código** y **el control de errores.**"*


En cuanto al control de errores cuando la función detecta un error devuelve null, al igual que hace la función estandar de php array_keys, el control de errores también se pude hacer lanzando excepciones.

```
#!php

throw new Exception('array paramenter must be type array') 
```

Esta linea de código está comentada en la clase StandarPhp para resaltar otra forma de tratamiento de errores, en este caso no se ha tratado así para que el comportamiento sea exactamente el mismo que el de la función estandar de php array_keys