<?php
/**
 * StandarPhp array_keys Implementation
 *
 * @author Florindo López Delgado
 *
 */
class StandarPhp
{

	/**
	* Return all the keys or a subset of the keys of an array
	*
	* Returns the keys, numeric and string, from the array. 
	* If the optional search_value is specified, then only 
	* the keys for that value are returned. Otherwise, 
	* all the keys from the array are returned.
	* 
	* @param array
	* @param mixed
	* @param boolean
	* @return array
	*
	*/
	public function array_keys($array, $search_value = null, $strict = false)
	{
		$result 	= null;
		$num_args 	= func_num_args();

		if(!$num_args || $num_args > 3){
			return $result;
		}

		if(is_array($array)){
			
			$result = array();
			foreach ($array as $key => $value) {

				if($num_args >= 2){
					if($strict){
						if($search_value === $value){
							$result[] = $key;
						}
					}else{
						if($search_value == $value){
							$result[] = $key;
						}
					}
				}else{
					$result[] = $key;
				}			
				
			}
		}else{

    		// throw new Exception('array paramenter must be type array');
		}

		return $result;
	}

} 