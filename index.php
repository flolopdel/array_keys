<?php

/** Include StandarPhp */
require_once 'StandarPhp.php';

$standarPhp = new StandarPhp();

$search_value 	= "world";
$strict 		= false;

$array 			= array("foo", "bar", "hello", "world");

echo "----------------ARRAY_KEYS----------------".PHP_EOL;

var_dump(array_keys($array, $search_value, $strict));

echo PHP_EOL."-----------OWN ARRAY_KEYS-------------". PHP_EOL;

var_dump($standarPhp->array_keys($array, $search_value, $strict));